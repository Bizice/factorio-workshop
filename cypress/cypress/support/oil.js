Cypress.Commands.add('findFirstOil', (playerName, options = {}) => {
    return cy.get('td > div > img[alt="Icon of Oil"]:first');
});

Cypress.Commands.add('BuildPetroleumFactory', (playerName, options = {}) => {
    cy.get('#action-BuildPetroleumFactory').click();
});

Cypress.Commands.add('BuildBasicFuelFactory', (playerName, options = {}) => {
    cy.get('#action-BuildBasicFuelFactory').click();
});

Cypress.Commands.add('BuildSolidFuelFactory', (playerName, options = {}) => {
    cy.get('#action-BuildSolidFuelFactory').click();
});

Cypress.Commands.add('BuildRocketFuelFactory', (playerName, options = {}) => {
    cy.get('#action-BuildRocketFuelFactory').click();
});

Cypress.Commands.add('OilDeposit', (options = {}) => {
    return cy.get('#bank-Oil-amount').invoke('text');
});

Cypress.Commands.add('basicFuelDeposit', (options = {}) => {
    return cy.get('#bank-BasicFuel-amount').invoke('text');
});

Cypress.Commands.add('solidFuelDeposit', (options = {}) => {
    return cy.get('#bank-SolidFuel-amount').invoke('text');
});

Cypress.Commands.add('rocketFuelDeposit', (options = {}) => {
    return cy.get('#bank-RocketFuel-amount').invoke('text');
});