
Cypress.Commands.add('findFirstIronOre', (playerName, options = {}) => {
    return cy.get('td > div > img[alt="Icon of IronOre"]:first');
});

Cypress.Commands.add('BuildIronPlateFactory', (playerName, options = {}) => {
    cy.get('#action-BuildIronPlateFactory').click();
});

Cypress.Commands.add('BuildSteelFactory', (playerName, options = {}) => {
    cy.get('#action-BuildSteelFactory').click();
});

Cypress.Commands.add('BuildSteelCasingFactory', (playerName, options = {}) => {
    cy.get('#action-BuildSteelCasingFactory').click();
});

Cypress.Commands.add('BuildRocketShellFactory', (playerName, options = {}) => {
    cy.get('#action-BuildRocketShellFactory').click();
});

Cypress.Commands.add('ironDeposit', (options = {}) => {
    return cy.get('#bank-IronOre-amount').invoke('text');
});

Cypress.Commands.add('ironPlateDeposit', (options = {}) => {
    return cy.get('#bank-IronPlate-amount').invoke('text');
});

Cypress.Commands.add('steelDeposit', (options = {}) => {
    return cy.get('#bank-Steel-amount').invoke('text');
});

Cypress.Commands.add('steelCasingDeposit', (options = {}) => {
    return cy.get('#bank-SteelCasing-amount').invoke('text');
});

Cypress.Commands.add('rocketShellDeposit', (options = {}) => {
    return cy.get('#bank-RocketShell-amount').invoke('text');
});