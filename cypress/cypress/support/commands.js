// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add('startGame', (playerName, options = {}) => {
    cy.visit('/');
    cy.get('select').select(playerName);
    cy.get('button').click(); // play as default player
});

Cypress.Commands.add('tickGame', (playerName, options = {}) => {
    cy.wait(1000); // game ticks like that
});

Cypress.Commands.add('stoneDeposit', (options = {}) => {
    return cy.get('#bank-Stone-amount').invoke('text');
});

Cypress.Commands.add('findFirstStone', (playerName, options = {}) => {
    return cy.get('td > div > img[alt="Stone Icon"]:first');
});

Cypress.Commands.add('findFirstCoal', (playerName, options = {}) => {
    return cy.get('td > div > img[alt="Icon of Coal"]:first');
});


