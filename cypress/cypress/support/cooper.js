Cypress.Commands.add('findFirstCopperOre', (playerName, options = {}) => {
    return cy.get('td > div > img[alt="Icon of CopperOre"]:first');
});

Cypress.Commands.add('BuildCopperPlateFactory', (playerName, options = {}) => {
    cy.get('#action-BuildCopperPlateFactory').click();
});

Cypress.Commands.add('BuildWireFactory', (playerName, options = {}) => {
    cy.get('#action-BuildWireFactory').click();
});

Cypress.Commands.add('BuildCircuitFactory', (playerName, options = {}) => {
    cy.get('#action-BuildCircuitFactory').click();
});

Cypress.Commands.add('BuildComputerFactory', (playerName, options = {}) => {
    cy.get('#action-BuildComputerFactory').click();
});

Cypress.Commands.add('copperDeposit', (options = {}) => {
    return cy.get('#bank-CopperOre-amount').invoke('text');
});

Cypress.Commands.add('copperPlateDeposit', (options = {}) => {
    return cy.get('#bank-CopperPlate-amount').invoke('text');
});

Cypress.Commands.add('wireDeposit', (options = {}) => {
    return cy.get("#bank-Wire-amount").invoke('text');
});

Cypress.Commands.add('circuitDeposit', (options = {}) => {
    return cy.get('#bank-CircuitCasing-amount').invoke('text');
});

Cypress.Commands.add('computerDeposit', (options = {}) => {
    return cy.get('#bank-Computer-amount').invoke('text');
});