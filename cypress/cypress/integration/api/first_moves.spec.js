context('Mining by hand', () => {
    beforeEach(() => {
        cy.startGame('Berlin');

        for (let i = 0; i < 5; i++) {
            cy.findFirstStone().click();
            cy.mineResource();
            cy.tickGame();
        }
        // let stoneState =  cy.stoneDeposit();
        cy.stoneDeposit().should('be.above', 4);

        while ( cy.stoneDeposit().should('be.above', 30)){
            cy.findFirstOil().click();
            cy.buildMine();
            cy.tickGame();
        }
    });

    it('Init Stone Production!', function () {
        for (let i = 0; i < 5; i++) {
            cy.findFirstStone().click();
            cy.mineResource();
            cy.tickGame();
        }
       // let stoneState =  cy.stoneDeposit();
        cy.stoneDeposit().should('be.above', 2);
        while ( cy.stoneDeposit().should('be.above', 30)){
            cy.tickGame();
        }
    });

   /* it('Init Iron Production!', function () {
        for (let i = 0; i < 2; i++) {
            cy.findFirstIronOre().click();
            cy.mineResource();
            cy.tickGame();
        }
        cy.findFirstIronOre().click();
        cy.buildMine();

    });*/

   /* it('Init Copper Production!', function () {
        for (let i = 0; i < 2; i++) {
            cy.findFirstCopperOre().click();
            cy.mineResource();
            cy.tickGame();
        }
        cy.findFirstCopperOre().click();
        cy.buildMine();

    });*/

    it('Init Oil Production!', function () {
        for (let i = 0; i < 2; i++) {
            cy.findFirstOil().click();
            cy.mineResource();
            cy.tickGame();
        }
        cy.findFirstOil().click();
        cy.buildMine();

    });
});