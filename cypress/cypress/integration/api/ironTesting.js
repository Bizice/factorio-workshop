context('Mining iron', () => {
    beforeEach(() => {

        cy.startGame('Berlin');

        for (let i = 0; i < 5; i++) {
            cy.findFirstStone().click();
            cy.mineResource();
            cy.tickGame();
        }

        cy.findFirstStone().click();
        cy.buildMine();
        // let stoneState =  cy.stoneDeposit();
        cy.stoneDeposit().should('be.above', 2);
        while ( cy.stoneDeposit().should('be.above', 30)){
            cy.tickGame();
        }

    });

    it('Init Iron Production!', function () {
        for (let i = 0; i < 2; i++) {
            cy.findFirstIronOre().click();
            cy.mineResource();
            cy.tickGame();
        }
        cy.findFirstIronOre().click();
        cy.buildMine();

    });
});