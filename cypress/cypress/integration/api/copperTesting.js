context('Mining copper', () => {
    beforeEach(() => {
        cy.startGame('Berlin');
    });

    it('Init Copper Production!', function () {
        for (let i = 0; i < 5; i++) {
            cy.findFirstCopperOre().click();
            cy.mineResource();
            cy.tickGame();
        }
        cy.findFirstCopperOre().click();
        cy.buildMine();

    });
});